#include "expr.hpp"
#include "expr_impl.hpp"
#include "tokenizer.hpp"
#include <stdexcept>
#include <queue>
#include <stack>
#include <sstream>

const expr expr::ZERO = expr::number(0.0);
const expr expr::ONE = expr::number(1.0);

// TODO: overloaded operators +, -, *, /, functions pow, log, sin, cos,
//       expr::number, expr::variable, operator==, operator<<,
//       create_expression_tree
bool isOperation(TokenId id) {
    return id == TokenId::Plus || id == TokenId::Minus || id == TokenId::Multiply || id == TokenId::Divide ||
           id == TokenId::Power;
}


bool isFunction(Token token) {
    return token.id == TokenId::Identifier &&
           (token.identifier == "sin" || token.identifier == "cos" || token.identifier == "log");
}

bool isParen(TokenId id) {
    return id == TokenId::LParen || id == TokenId ::RParen;
}

std::queue<Token> create_token_postorder_queue(const std::string &expression) {
    auto tmp_stack = std::stack<Token>();
    auto result_queue = std::queue<Token>();

    auto stringstream = std::stringstream(expression);
    auto tokenizer = Tokenizer(stringstream);

    auto last_token = Token(TokenId::End);
    auto tmp_token = Token(TokenId::End);

    while (true) {
        Token token = tokenizer.next();

        if (tmp_token.id != TokenId::End) {
            if (token.id == TokenId::LParen) {
                tmp_stack.push(tmp_token);
            } else if (isOperation(token.id) || token.id == TokenId::End) {
                if ((!tmp_stack.empty() && !isOperation(last_token.id) && last_token.id != TokenId::LParen) || last_token.id == TokenId::RParen) throw parse_error("kek error");
                tmp_token.number = 1.0;
                result_queue.push(tmp_token);
            } else {
                throw parse_error("weeeird_error");
            }
            tmp_token = Token(TokenId::End);
        }

        if (token.id == TokenId::End) break;

        if (token.id == TokenId::Number) {
            if (!tmp_stack.empty() && !isOperation(last_token.id) && !isParen(last_token.id)) throw parse_error("kek error");
            result_queue.push(token);
        }

        if (isOperation(token.id)) {
            while (!tmp_stack.empty() && tmp_stack.top().id != TokenId::LParen
                   && (isFunction(tmp_stack.top())
                       || (tmp_stack.top().op_precedence() > token.op_precedence()
                           || (tmp_stack.top().op_precedence() == token.op_precedence() &&
                               token.associativity() == Associativity::Left))
                   )) {
                Token popped_token = tmp_stack.top();
                result_queue.push(popped_token);
                tmp_stack.pop();
            }
            tmp_stack.push(token);
        }

        if (token.id == TokenId::LParen) {
            if (last_token.id == TokenId::Identifier && !isFunction(last_token)) throw unknown_function_exception("Such function doesn't exist");
            tmp_stack.push(token);
        }

        if (token.id == TokenId::RParen) {
            if (!tmp_stack.empty() && last_token.id == TokenId::LParen) throw parse_error("Nothing inside parens");
            while (!tmp_stack.empty() && tmp_stack.top().id != TokenId::LParen) {
                Token popped_token = tmp_stack.top();
                result_queue.push(popped_token);
                tmp_stack.pop();
            }
            if (tmp_stack.empty()) throw parse_error("Missing left paren");
            tmp_stack.pop();
        }

        if (token.id == TokenId::Identifier) {
            if (!isFunction(token)) {
                if ((!tmp_stack.empty() && !isOperation(last_token.id) && last_token.id != TokenId::LParen) || last_token.id == TokenId::RParen) throw parse_error("kek error");
                result_queue.push(token);
            } else {
                tmp_token = token;
            }
        }

        last_token = token;
    }

    while (!tmp_stack.empty()) {
        Token popped_token = tmp_stack.top();
        if (isParen(popped_token.id)) throw parse_error("wrong parens");
        result_queue.push(popped_token);
        tmp_stack.pop();
    }

    return result_queue;

}

expr create_expression_tree(const std::string &expression) {
    auto token_queue = create_token_postorder_queue(expression);
    auto stack = std::stack<expr>();

    while (!token_queue.empty()) {
        auto expr = token_queue.front();

        if (expr.id == TokenId::Number) {
            auto expr2 = expr::number(expr.number);
            stack.push(expr2);
            token_queue.pop();
        } else if (expr.id == TokenId::Plus) {
            if (stack.size() < 2) throw parse_error("Not enough operands");
            auto expr2 = stack.top();
            stack.pop();
            auto expr1 = stack.top();
            stack.pop();
            stack.push(expr1 + expr2);
            token_queue.pop();
        } else if (expr.id == TokenId::Minus) {
            if (stack.size() < 2) throw parse_error("Not enough operands");
            auto expr2 = stack.top();
            stack.pop();
            auto expr1 = stack.top();
            stack.pop();
            stack.push(expr1 - expr2);
            token_queue.pop();
        } else if (expr.id == TokenId::Divide) {
            if (stack.size() < 2) throw parse_error("Not enough operands");
            auto expr2 = stack.top();
            stack.pop();
            auto expr1 = stack.top();
            stack.pop();
            stack.push(expr1 / expr2);
            token_queue.pop();
        } else if (expr.id == TokenId::Multiply) {
            if (stack.size() < 2) throw parse_error("Not enough operands");
            auto expr2 = stack.top();
            stack.pop();
            auto expr1 = stack.top();
            stack.pop();
            stack.push(expr1 * expr2);
            token_queue.pop();
        } else if (expr.id == TokenId::Power) {
            if (stack.size() < 2) throw parse_error("Not enough operands");
            auto expr2 = stack.top();
            stack.pop();
            auto expr1 = stack.top();
            stack.pop();
            stack.push(pow(expr1, expr2));
            token_queue.pop();
        } else if (expr.id == TokenId::Identifier) {
            if (isFunction(expr) && expr.number == 0) {
                if (stack.size() < 1) throw parse_error("Not enough operands");
                auto expr1 = stack.top();
                stack.pop();
                if (expr.identifier == "sin") stack.push(sin(expr1));
                else if (expr.identifier == "cos") stack.push(cos(expr1));
                else if (expr.identifier == "log") stack.push(log(expr1));
                token_queue.pop();
            } else {
                auto expr2 = expr::variable(expr.identifier);
                stack.push(expr2);
                token_queue.pop();
            }
        } else {
            throw tokenize_error("unknown token");
        }
    }

    return stack.top();
}

bool operator==(const expr &a, const expr &b) {
    return a->equals(b->shared_from_this().operator*());
}

std::ostream &operator<<(std::ostream &os, const expr &e) {
    e->write(os, expr::WriteFormat::Prefix);
    return os;
}

expr expr::number(double n) {
    return std::make_shared<exprs::number>(exprs::number(n));
}

expr expr::variable(const std::string &name) {
    return std::make_shared<exprs::variable>(exprs::variable(name));
}

expr operator+(const expr &first, expr second) {
    return std::make_shared<exprs::expr_plus>(exprs::expr_plus(first, second));
}

expr operator-(expr first, expr second) {
    return std::make_shared<exprs::expr_minus>(exprs::expr_minus(first, second));
}

expr operator*(expr first, expr second) {
    return std::make_shared<exprs::expr_multiply>(exprs::expr_multiply(first, second));
}

expr operator/(expr first, expr second) {
    return std::make_shared<exprs::expr_divide>(exprs::expr_divide(first, second));
}

expr pow(expr first, expr second) {
    return std::make_shared<exprs::expr_pow>(exprs::expr_pow(first, second));
}

expr sin(expr value) {
    return std::make_shared<exprs::expr_sin>(exprs::expr_sin(value));
}

expr cos(expr value) {
    return std::make_shared<exprs::expr_cos>(exprs::expr_cos(value));
}

expr log(expr value) {
    return std::make_shared<exprs::expr_log>(exprs::expr_log(value));
}