#pragma once

#include "expr.hpp"
#include <iosfwd>

namespace exprs {

    class expr_plus : public expr_base {
    public:
        expr_plus(const expr &, const expr &);

    private:
        virtual double evaluate(const variable_map_t &variables) const override;

        virtual expr derive(std::string const &variable) const override;

        virtual expr simplify() const override;

        virtual void write(std::ostream &out, WriteFormat fmt) const override;

        virtual bool equals(const expr_base &b) const override;

        expr first;
        expr second;
    };


    class expr_minus : public expr_base {
    public:
        expr_minus(const expr &, const expr &);

    private:
        virtual double evaluate(const variable_map_t &variables) const override;

        virtual expr derive(std::string const &variable) const override;

        virtual expr simplify() const override;

        virtual void write(std::ostream &out, WriteFormat fmt) const override;

        virtual bool equals(const expr_base &b) const override;

        expr first;
        expr second;
    };


    class expr_multiply : public expr_base {
    public:
        expr_multiply(const expr &, const expr &);

    private:
        virtual double evaluate(const variable_map_t &variables) const override;

        virtual expr derive(std::string const &variable) const override;

        virtual expr simplify() const override;

        virtual void write(std::ostream &out, WriteFormat fmt) const override;

        virtual bool equals(const expr_base &b) const override;

        expr first;
        expr second;
    };


    class expr_divide : public expr_base {
    public:
        expr_divide(const expr &, const expr &);

    private:
        double evaluate(const variable_map_t &variables) const override;

        expr derive(std::string const &variable) const override;

        expr simplify() const override;

        void write(std::ostream &out, WriteFormat fmt) const override;

        bool equals(const expr_base &b) const override;

        expr first;
        expr second;
    };


    class expr_pow : public expr_base {
    public:
        expr_pow(const expr &, const expr &);

    private:
        double evaluate(const variable_map_t &variables) const override;

        expr derive(std::string const &variable) const override;

        expr simplify() const override;

        void write(std::ostream &out, WriteFormat fmt) const override;

        bool equals(const expr_base &b) const override;

        expr first;
        expr second;
    };


    class expr_sin : public expr_base {
    public:
        explicit expr_sin(const expr &);

    private:
        double evaluate(const variable_map_t &variables) const override;

        expr derive(std::string const &variable) const override;

        expr simplify() const override;

        void write(std::ostream &out, WriteFormat fmt) const override;

        bool equals(const expr_base &b) const override;

        expr value;
    };


    class expr_cos : public expr_base {
    public:
        explicit expr_cos(const expr &);

    private:
        double evaluate(const variable_map_t &variables) const override;

        expr derive(std::string const &variable) const override;

        expr simplify() const override;

        void write(std::ostream &out, WriteFormat fmt) const override;

        bool equals(const expr_base &b) const override;

        expr value;
    };


    class expr_log : public expr_base {
    public:
        explicit expr_log(const expr &);

    private:
        double evaluate(const variable_map_t &variables) const override;

        expr derive(std::string const &variable) const override;

        expr simplify() const override;

        void write(std::ostream &out, WriteFormat fmt) const override;

        bool equals(const expr_base &b) const override;

        expr value;
    };


    class number : public expr_base {
    public:
        explicit number(double);

    private:
        double evaluate(const variable_map_t &variables) const override;

        expr derive(std::string const &variable) const override;

        expr simplify() const override;

        void write(std::ostream &out, WriteFormat fmt) const override;

        bool equals(const expr_base &b) const override;

        double value;
    };


    class variable : public expr_base {
    public:
        explicit variable(const std::string&);

    private:
        double evaluate(const variable_map_t &variables) const override;

        expr derive(std::string const &variable) const override;

        expr simplify() const override;

        void write(std::ostream &out, WriteFormat fmt) const override;

        bool equals(const expr_base &b) const override;

        std::string name;
    };
}
