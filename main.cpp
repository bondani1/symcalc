#include <iostream>
#include "cmdline.hpp" // parse_command
#include "app.hpp" // handle_expr_line
#include "expr.hpp"

int main(int argc, char *argv[]) {
    std::string expression;
    std::vector<Commands::Command> commands;
    for (int i = 1; i < argc; i++) {
        commands.push_back(parse_command(argv[i]));
    }

    while (getline(std::cin, expression)) {
        try {
            handle_expr_line(std::cout, expression, commands);
        } catch (std::exception e) {
            std::cout << "!" << e.what() << std::endl;
        }
    }
/*    auto x = "sin";
    auto q = create_token_postorder_queue(x);
    auto tree = create_expression_tree(x);
    std::cout << tree  << std::endl;

    while (!q.empty())
    {   std::string x;
        if (q.front().id == TokenId::Number) x = q.front().number;
        if (q.front().id == TokenId::Identifier) x = q.front().identifier;
        else x = static_cast<char>(q.front().id);
        std::cout << x << " ";
        q.pop();
    }
    std::cout << std::endl;

 */
}
