#include "expr_impl.hpp"

#include <iostream>
#include <cmath>
#include <limits>

namespace exprs {

    // PLUS methods -----------------------------------------------------
    expr_plus::expr_plus(const expr &first, const expr &second) : first(first), second(second) {}

    double expr_plus::evaluate(const expr_base::variable_map_t &variables) const {
        double left = first->evaluate(variables);
        double right = second->evaluate(variables);

        return left + right;
    }

    expr expr_plus::derive(std::string const &variable) const {
        expr left = first->derive(variable);
        expr right = second->derive(variable);

        return left + right;
    }

    expr expr_plus::simplify() const {
        expr left = first->simplify();
        expr right = second->simplify();

        if (left == expr::ZERO) {
            return right;
        } else if (right == expr::ZERO) {
            return left;
        } else {
            return left + right;
        }

    }

    void expr_plus::write(std::ostream &out, expr_base::WriteFormat fmt) const {
        out << "(+ " << first << " " << second << ")";
    }

    bool expr_plus::equals(const expr_base &b) const {
        const expr_plus *tmp = dynamic_cast<expr_plus const *>(b.shared_from_this().get());
        return tmp ? tmp->first == first && tmp->second == second : false;
    }

    // MINUS methods -----------------------------------------------------
    expr_minus::expr_minus(const expr &first, const expr &second) : first(first), second(second) {}

    double expr_minus::evaluate(const expr_base::variable_map_t &variables) const {
        double left = first->evaluate(variables);
        double right = second->evaluate(variables);

        return left - right;
    }

    expr expr_minus::derive(std::string const &variable) const {
        expr left = first->derive(variable);
        expr right = second->derive(variable);

        return left - right;
    }

    expr expr_minus::simplify() const {
        expr left = first->simplify();
        expr right = second->simplify();
        if (right == expr::ZERO) {
            return left;
        }
        return left - right;
    }

    void expr_minus::write(std::ostream &out, expr_base::WriteFormat fmt) const {
        out << "(- " << first << " " << second << ")";
    }

    bool expr_minus::equals(const expr_base &b) const {
        const expr_minus *tmp = dynamic_cast<expr_minus const *>(b.shared_from_this().get());
        return tmp ? tmp->first == first && tmp->second == second : false;
    }

    // MULTIPLY methods -----------------------------------------------------
    expr_multiply::expr_multiply(const expr &first, const expr &second) : first(first), second(second) {}

    double expr_multiply::evaluate(const expr_base::variable_map_t &variables) const {
        double left = first->evaluate(variables);
        double right = second->evaluate(variables);

        return left * right;
    }

    expr expr_multiply::derive(std::string const &variable) const {
        expr left = first->derive(variable);
        expr right = second->derive(variable);

        return left * second + first * right;
    }

    expr expr_multiply::simplify() const {
        expr left = first->simplify();
        expr right = second->simplify();
        if (left == expr::ZERO || right == expr::ZERO) {
            return expr::ZERO;
        } else if (left == expr::ONE) {
            return right;
        } else if (right == expr::ONE) {
            return left;
        }
        return left * right;;
    }

    void expr_multiply::write(std::ostream &out, expr_base::WriteFormat fmt) const {
        out << "(* " << first << " " << second << ")";
    }

    bool expr_multiply::equals(const expr_base &b) const {
        const expr_multiply *tmp = dynamic_cast<expr_multiply const *>(b.shared_from_this().get());
        return tmp ? tmp->first == first && tmp->second == second : false;
    }

    //DIVIDE methods -----------------------------------------------------
    expr_divide::expr_divide(const expr &first, const expr &second) : first(first), second(second) {}

    double expr_divide::evaluate(const expr_base::variable_map_t &variables) const {
        double left = first->evaluate(variables);
        double right = second->evaluate(variables);

        return left / right;
    }

    expr expr_divide::derive(std::string const &variable) const {
        expr left = first->derive(variable);
        expr right = second->derive(variable);

        return (left * second - first * right) / (pow(second, expr::number(2)));
    }

    expr expr_divide::simplify() const {
        expr left = first->simplify();
        expr right = second->simplify();

        if (left == expr::ZERO && right == expr::ZERO) {
            return left / right;
        } else if (right == expr::ONE) {
            return left;
        }
        if (left == expr::ZERO) {
            return expr::ZERO;
        }

        return left / right;
    }

    void expr_divide::write(std::ostream &out, expr_base::WriteFormat fmt) const {
        out << "(/ " << first << " " << second << ")";
    }

    bool expr_divide::equals(const expr_base &b) const {
        const expr_divide *tmp = dynamic_cast<expr_divide const *>(b.shared_from_this().get());
        return tmp ? tmp->first == first && tmp->second == second : false;
    }

    //POWER methods -----------------------------------------------------
    expr_pow::expr_pow(const expr &first, const expr &second) : first(first), second(second) {}

    double expr_pow::evaluate(const expr_base::variable_map_t &variables) const {
        double left = first->evaluate(variables);
        double right = second->evaluate(variables);

        return pow(left, right);
    }

    expr expr_pow::derive(std::string const &variable) const {
        expr left = first->derive(variable);
        expr right = second->derive(variable);
        return pow(first, second) * (((left * second) / first) + log(first) * right);
    }

    expr expr_pow::simplify() const {
        expr left = first->simplify();
        expr right = second->simplify();

        if (right == expr::ONE) {
            return left;
        } else if (right == expr::ZERO) {
            return expr::ONE;
        } else if (left == expr::ZERO) {
            return expr::ZERO;
        } else return pow(left, right);
    }

    void expr_pow::write(std::ostream &out, expr_base::WriteFormat fmt) const {
        out << "(^ " << first << " " << second << ")";
    }

    bool expr_pow::equals(const expr_base &b) const {
        const expr_pow *tmp = dynamic_cast<expr_pow const *>(b.shared_from_this().get());
        return tmp ? tmp->first == first && tmp->second == second : false;
    }

    //SIN methods -----------------------------------------------------
    expr_sin::expr_sin(const expr &value) : value(value) {}

    double expr_sin::evaluate(const expr_base::variable_map_t &variables) const {
        double result = value->evaluate(variables);

        return sin(result);
    }

    expr expr_sin::derive(std::string const &variable) const {
        expr result = cos(value) * value->derive(variable);

        return result;
    }

    expr expr_sin::simplify() const {
        return sin(value->simplify());
    }

    void expr_sin::write(std::ostream &out, expr_base::WriteFormat fmt) const {
        out << "(sin " << value << ")";
    }

    bool expr_sin::equals(const expr_base &b) const {
        const expr_sin *tmp = dynamic_cast<expr_sin const *>(b.shared_from_this().get());
        return tmp ? tmp->value == value : false;
    }

    //COS methods -----------------------------------------------------
    expr_cos::expr_cos(const expr &value) : value(value) {}

    double expr_cos::evaluate(const expr_base::variable_map_t &variables) const {
        double result = value->evaluate(variables);

        return cos(result);
    }

    expr expr_cos::derive(std::string const &variable) const {
        expr result = (expr::ZERO - sin(value)) * value->derive(variable);

        return result;
    }

    expr expr_cos::simplify() const {
        return cos(value->simplify());
    }

    void expr_cos::write(std::ostream &out, expr_base::WriteFormat fmt) const {
        out << "(cos " << value << ")";
    }

    bool expr_cos::equals(const expr_base &b) const {
        const expr_cos *tmp = dynamic_cast<expr_cos const *>(b.shared_from_this().get());
        return tmp ? tmp->value == value : false;
    }

    //LOG methods -----------------------------------------------------
    expr_log::expr_log(const expr &value) : value(value) {}

    double expr_log::evaluate(const expr_base::variable_map_t &variables) const {
        double result = value->evaluate(variables);

        if (result <= 0) {
            throw domain_exception("log can't be lower than O");
        } else {
            return log(result);
        }

    }

    expr expr_log::derive(std::string const &variable) const {
        expr result = value->derive(variable) / value;

        return result;
    }

    expr expr_log::simplify() const {
        expr tmp = value->simplify();

        if (tmp == expr::ONE) {
            return expr::ZERO;
        } else return log(tmp);
    }

    void expr_log::write(std::ostream &out, expr_base::WriteFormat fmt) const {
        out << "(log " << value << ")";
    }

    bool expr_log::equals(const expr_base &b) const {
        const expr_log *tmp = dynamic_cast<expr_log const *>(b.shared_from_this().get());
        return tmp ? tmp->value == value : false;
    }

    //NUMBER methods -----------------------------------------------------
    number::number(double value) : value(value) {}

    double number::evaluate(const expr_base::variable_map_t &variables) const {
        return value;
    }

    expr number::derive(std::string const &variable) const {
        return expr::ZERO;
    }

    expr number::simplify() const {
        return expr::number(value);
    }

    void number::write(std::ostream &out, expr_base::WriteFormat fmt) const {
        out << value;
    }

    bool number::equals(const expr_base &b) const {
        const number *v = dynamic_cast<number const *>(b.shared_from_this().get());
        return v ? v->value == value : false;
    }

    //VARIABLE methods -----------------------------------------------------
    variable::variable(const std::string &name) : name(name) {}

    double variable::evaluate(const expr_base::variable_map_t &variables) const {
        if (variables.find(name) == variables.end()) {
            throw unbound_variable_exception("Variable doesn't exist");
        }

        return variables.at(name);
    }

    expr variable::derive(std::string const &variable) const {
        if (name == variable) {
            return expr::ONE;
        } else {
            return expr::ZERO;
        }
    }

    expr variable::simplify() const {
        return expr::variable(name);
    }

    void variable::write(std::ostream &out, expr_base::WriteFormat fmt) const {
        out << name;
    }

    bool variable::equals(const expr_base &b) const {
        const variable *tmp = dynamic_cast<variable const *>(b.shared_from_this().get());
        return tmp ? tmp->name == name : false;
    }
} // namespace exprs
